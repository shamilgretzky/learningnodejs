//Test to see if function returns hello

const assert = require('chai').assert;
const index = require('../index'); //Going outside of test folder so ../index

//could comment below and bring back above ** (then say: let result = index.sayHello();)

//**
//const sayHello = require('../index').sayHello;
//const addNumbers = require('../index').addNumbers;
//**

/*
describe('App', function(){
it('index should return hello', function(){     //Description of what you want it to test then function
  assert.equal(index(), 'hello'); //https://www.chaijs.com/api/assert/
  });
});
*/

//***Make a Global Scope here so, Results =
sayHelloResult = index.sayHello();
addNumbersResult = index.addNumbers(5,5);

describe('App', function(){
  describe('sayHello()', function(){
    it('sayHello should return hello', function(){     //Description of what you want it to test then function
      //***let result = index.sayHello();
      //***assert.equal(result, 'hello'); //https://www.chaijs.com/api/assert/
        assert.equal(sayHelloResult, 'hello');
      });
      //Want to see Hello is type string
      it('sayHello should return type string', function(){
        //***let result = index.sayHello();
        //***assert.typeOf(result, 'string', "It should be a string" );
        assert.typeOf(sayHelloResult, 'string', "It should be a string" );
      });
  });

  describe("addNumbers()", function(){
    it('addNumbers should be abouve 5', function(){
      //***let result = index.addNumbers(5,5);
      //***assert.isAbove(result, 5, 'Sum should be greater than 5')
      assert.isAbove(addNumbersResult, 5, 'Sum should be greater than 5')
    });

  //Make sure it is a Number
    it('addNumbers should return type Number', function(){
      //***let result = index.addNumbers(5,5);
      //***assert.isNumber(result, 'Should be a number')
      assert.isNumber(addNumbersResult, 'Should be a number')
    });
    //Make sure it is a Number
      it('addNumbers should return type Number', function(){
        //***let result = index.addNumbers(5,5);
        //***assert.typeOf(result, 'number')
        assert.typeOf(addNumbersResult, 'number')
      });
  });



});
